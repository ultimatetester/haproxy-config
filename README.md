HAProxy Config
===================
This module can read, modify and write config files that are compatible with HAProxy with 0 dependencies.
Every line of configuration will be checked for validity by key (value can be anything).

Loading Config
-------------
To load an existing configuration file, just use the constructor:

    var HAProxyConfig = require('haproxy-config');
    var haproxyConfig = new HAProxyConfig('/etc/haproxy/haproxy.cfg');
    haproxyConfig.loadConfig(function(err) {
        if(!!err) {
            // Handle this
        }
        
        // haproxyConfig is now filled with the existing configuration
    });

Creating or Modifying Config
-------------
If you want to create a configuration from scratch or want to modify an existing configuration file, you can use the following functions:

**haproxyConfig.addSection(type, name)** Used to add a section to the configuration file. The type can be any of 'global', 'defaults', 'frontend', 'listen' or 'backend'. Not checked is the name and can be anything. This function returns a section object to which you can add a configuration.

**mySection.addConfig(key, value)** Used to add a configuration line to the section. For validity, the key will be checked (does the key exist and is it allowed within this section). A full list of valid keys can be found here (not supported are deprecated keys): http://cbonte.github.io/haproxy-dconv/1.7/configuration.html#4.1
The value can be anything and is not checked.

Saving Config
-------------
When you are done created your configuration file, it can be saved by calling the following command:

    haproxyConfig.save(function(err) {
        if(!!err) {
            // Handle this
        }
    });
    
The configuration will be saved to the path given in the constructor. If the file exists it will be overwritten.
