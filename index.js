const fs = require('fs');
const readline = require('readline');
const allowedKeywords = require('./allowedKeywords');

/**
 *
 * @callback errorOnlyCallback
 * @param {Error|object} err
 */

/**
 * Represents a HAProxy Config object
 * @param {string} fileLocation
 * @constructor
 */
const HAProxyConfig = function (fileLocation) {
    this.fileLocation = fileLocation;
    this.global = {};
    this.defaults = {};
    this.frontends = [];
    this.backends = [];
};

/**
 *
 * @param {string} type
 * @param {string} name
 * @returns {object}
 */
HAProxyConfig.prototype.addSection = function (type, name) {
    if (allowedKeywords.sections.indexOf(type) === -1) {
        throw new Error(type + ' is not allowed!');
    }

    const self = this;
    if (type === 'global') {
        self.global._type = 'global';
        self.global._name = name || '';
        self.global.addConfig = function (key, value) {
            self.addConfig(this, key, value);
        };

        return self.global;
    } else if (type === 'defaults') {
        self.defaults._type = 'defaults';
        self.defaults._name = name || '';
        self.defaults.addConfig = function (key, value) {
            self.addConfig(this, key, value);
        };

        return self.defaults;
    }

    const section = {};
    section._type = type;
    section._name = name || '';
    section.addConfig = function (key, value) {
        self.addConfig(this, key, value);
    };

    if (section._type === 'frontend') {
        this.frontends.push(section);
    } else if (section._type === 'backend') {
        this.backends.push(section);
    }

    return section;
};

/**
 *
 * @param {object} section
 * @param {string} key
 * @param {string} value
 */
HAProxyConfig.prototype.addConfig = function (section, key, value) {
    if (typeof section[key] != 'undefined') {
        if (Array.isArray(section[key]) === false) {
            section[key] = [section[key]];
        }

        section[key].push((value || '').trim());
        return;
    }

    section[key.trim()] = (value || '').trim();
};

/**
 *
 * @param {errorOnlyCallback} cb
 */
HAProxyConfig.prototype.loadConfig = function (cb) {
    const self = this;

    const fileStream = fs.createReadStream(self.fileLocation);
    fileStream.on('error', function (err) {
        cb(err);
    });

    const lineReader = readline.createInterface({
        input: fileStream
    });

    let currentSection = {};
    lineReader.on('line', function (line) {
        if (line.trim().length === 0) {
            return;
        }

        if (/^\s/.test(line) === false) {
            // this is a section!
            const columns = line.split(/\s+/g);
            currentSection = self.addSection(columns[0], columns[1]);

            if (currentSection == null) {
                currentSection = {};
            }
        } else {
            if (typeof currentSection._type == 'undefined') {
                return;
            }

            for (let i = 0; i < allowedKeywords[currentSection._type].length; i++) {
                const regexp = new RegExp('^(no|default)?(\\s+)?(' + allowedKeywords[currentSection._type][i] + ')(\\s+)?(.*)?$');
                const matches = regexp.exec(line);
                if (matches == null) {
                    continue;
                }

                currentSection.addConfig(matches[3], matches[5]);
            }
        }
    });

    lineReader.on('close', function () {
        cb(null);
    });
};

/**
 *
 * @param {object} section
 * @returns {string}
 */
HAProxyConfig.prototype.sectionToConfig = function (section) {
    let configuration = '';

    if (!section) {
        return '';
    }

    configuration += section._type + (section._name ? (' ' + section._name) : '') + '\n';
    for (const key in section) {
        if (section.hasOwnProperty(key) === false) {
            continue;
        }

        if (key === '_type' || key === '_name') {
            continue;
        }

        if (typeof section[key] == 'function') {
            continue;
        }

        if (Array.isArray(section[key])) {
            for (let i = 0; i < section[key].length; i++) {
                configuration += '\t' + key + (section[key][i] ? (' ' + section[key][i]) : '') + '\n';
            }

            continue;
        }

        configuration += '\t' + key + (section[key] ? (' ' + section[key]) : '') + '\n';
    }

    return configuration;
};

/**
 *
 * @param {errorOnlyCallback} cb
 */
HAProxyConfig.prototype.saveConfig = function (cb) {
    let configuration = '';
    configuration += this.sectionToConfig(this.global);
    configuration += this.sectionToConfig(this.defaults);

    for (let i = 0; i < this.frontends.length; i++) {
        configuration += this.sectionToConfig(this.frontends[i]);
    }

    for (let j = 0; j < this.backends.length; j++) {
        configuration += this.sectionToConfig(this.backends[j]);
    }

    fs.writeFile(this.fileLocation, configuration, {}, cb);
};

/**
 *
 * @param {string} name
 * @returns {object}
 */
HAProxyConfig.prototype.getFrontendByName = function (name) {
    for (let i = 0; i < this.frontends.length; i++) {
        const frontend = this.frontends[i];
        if (frontend._name === name) {
            return frontend;
        }
    }

    return null;
};

/**
 *
 * @param {string} name
 * @returns {object}
 */
HAProxyConfig.prototype.getBackendByName = function (name) {
    for (let i = 0; i < this.backends.length; i++) {
        const backend = this.backends[i];
        if (backend._name === name) {
            return backend;
        }
    }

    return null;
};

module.exports = HAProxyConfig;