const chai = require('chai');
const HAProxyConfig = require('../index');

describe('HAProxy', function () {
    let haProxyConfig = null;
    before(function () {
        haProxyConfig = new HAProxyConfig('nonexistent.cfg');
    });

    describe('#loadConfig()', function () {
        it('should catch the loading error', function (done) {

            haProxyConfig.loadConfig(function (err) {
                chai.assert.exists(err, 'error caught successfully');
                done();
            });

        });
    });
});
